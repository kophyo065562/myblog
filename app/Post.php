<?php

namespace App;
use App\User;
use App\Category; //call modal App

use Illuminate\Database\Eloquent\Model;
use App\Comment;

class Post extends Model
{
    //
    protected $fillable = ['title','category_id','user_id','photo','body'
];

	public function user(){
		return $this->belongsTo(User::class);
	}
	public function category(){
		return $this->belongsTo(Category::class);
	}
	public function comments(){
		return $this->hasMany(Comment::class);
	}

}
