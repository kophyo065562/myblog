<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;

use App\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Post::all();
        return view('blog',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        return view('post.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate(request(),[
            'title' => 'required|min:5',
            
            'photo' => 'image|mimes:jpeg,jpg.png',
            'body' => 'required|min:10'
        ]);

        $imgName = time().'.'.request()->photo->getClientOriginalExtension();
        request()->photo->move(public_path('images'),$imgName);
        $fullImg = '/images/' . $imgName;
        Post::create([
            'title' =>request('title'),
            'category_id' =>request('category'),
            'photo' => $fullImg,
            'body' => request('body'),
            'user_id' => auth()->id()
        ]);
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
        return view('post.blogpost',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
         $categories=Category::all();
         return view('post.edit',compact('post','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        
        $id=request('postid');
        $post=Post::find($id);
        $post->title=request('title');

        if (request('photo')){
            $imageName=time().'.'.request()->photo->
            getClientOriginalExtension();
            request()->photo->move(public_path('images'),$imageName);
            $fullImg='/images/'.$imageName;
            $post->photo=$fullImg;
        }
        
        $post->body=request('body');
        $post->category_id=request('category');
        $post->save();
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
        $post->delete();
        return redirect('/');/*let go to 80000*/ 
    }
}
