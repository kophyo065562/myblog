<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// post
Route::get('/','PostController@index');
Route::get('/upload','PostController@create');
Route::post('/add','PostController@store');
Route::get('/post/{post}','PostController@show');
Route::get('/post/delete/{post}','PostController@destroy');
Route::get('/post/edit/{post}','PostController@edit');
Route::post('post/update','PostController@update');


// category
Route::get('/categories','CategoryController@index');
/*category is in index*/
Route::get('/add_category','CategoryController@create');
Route::post('save_category','CategoryController@store');
Route::get('/category/delete/{category}','CategoryController@destroy');
Route::get('/category/edit/{category}','CategoryController@edit');
Route::post('/category/update','CategoryController@update');
//get is for a input or a tab


/*comment*/
Route::post('/comment',"CommentController@create");


/*Auth scaffolding*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*For Admin Talbe Auth*/
Route::get('admin-login','Auth\AdminLoginController@showLoginForm');//for to enter admin
Route::post('admin-login','Auth\AdminLoginController@login');
Route::get('/backend','AdminController@index');



